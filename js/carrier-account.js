(function ($, Drupal, drupalSettings) {
  "use strict";

  Drupal.commerceEasyPostUpdateCarrierAccountPane = function(element, carrier_account_pane, settings) {
    var shipping_method = $(element).attr('data-shipping-method');
    var carrier = $(element).attr('data-carrier');
    var carrier_name = carrier;
    if (settings.easypost_customer_carrier_account !== undefined &&
      settings.easypost_customer_carrier_account.carrier_names !== undefined &&
      settings.easypost_customer_carrier_account.carrier_names[carrier] !== undefined) {
      carrier_name = settings.easypost_customer_carrier_account.carrier_names[carrier];
    }
    if (settings.easypost_customer_carrier_account !== undefined &&
      settings.easypost_customer_carrier_account.shipping_methods[shipping_method] !== undefined &&
      settings.easypost_customer_carrier_account.shipping_methods[shipping_method][carrier] !== undefined &&
      settings.easypost_customer_carrier_account.shipping_methods[shipping_method][carrier].allow_customer_account) {
      var label = $(carrier_account_pane).find('label').first();
      $(label).text('Use your own ' + carrier_name + ' account')
      carrier_account_pane.show();
    }
    else {
      carrier_account_pane.hide();
      $(carrier_account_pane).find('input').each(function() {
        if ($(this).attr('type') === 'checkbox') {
          $(this).prop('checked', false);
        }
        else {
          $(this).val('');
        }
      });
    }
  };

  Drupal.behaviors.commerceEasyPostCarrierAccount = {
    attach: function (context, settings) {
      if (settings.easypost_customer_carrier_account !== undefined) {
        var parent = $('.field--widget-commerce-shipping-rate');
        var carrier_account_pane = $('*[data-drupal-selector="edit-commerce-easypost-carrier-account"]');
        $(parent).once('easypost-carrier-account').each(function() {
          var options = $(this).find('input[type="radio"]');
          var names = {};
          $(options).each(function() {
            var matches = $(this).val().match(/^(\d+)--EasyPost::(\w+)::(\w+)$/);
            if (matches.length >= 4) {
              $(this).attr('data-shipping-method', matches[1]);
              $(this).attr('data-carrier', matches[2]);
              $(this).attr('data-service', matches[3]);
              names[$(this).attr('name')] = $(this).attr('name');
            }
            if ($(this).prop('checked')) {
              Drupal.commerceEasyPostUpdateCarrierAccountPane(this, carrier_account_pane, settings);
            }
          });
          for (const [key, value] of Object.entries(names)) {
            $('input[name="' + key + '"]').bind('change', function() {
              Drupal.commerceEasyPostUpdateCarrierAccountPane(this, carrier_account_pane, settings);
            });
          }
        });
      }
    }
  };

})(jQuery, Drupal, drupalSettings);
