<?php

namespace Drupal\commerce_easypost\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_easypost\Service\EasyPostManager;
use Drupal\commerce_easypost\Service\EasyPostServiceDescriptions;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a pane for the customer to enter their own carrier account number.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_easypost_carrier_account",
 *   label = @Translation("Customer Carrier Account"),
 *   wrapper_element = "fieldset",
 * )
 */
class CustomerCarrierAccount extends CheckoutPaneBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\commerce_easypost\Service\EasyPostManager
   */
  protected $easyPostManager;

  /**
   * @var \Drupal\commerce_easypost\Service\EasyPostServiceDescriptions
   */
  protected $serviceDescriptions;

  /**
   * Constructs a new CheckoutPaneBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface $checkout_flow
   *   The parent checkout flow.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager, EasyPostManager $easyPostManager, EasyPostServiceDescriptions $serviceDescriptions) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);

    $this->easyPostManager = $easyPostManager;
    $this->serviceDescriptions = $serviceDescriptions;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow,
      $container->get('entity_type.manager'),
      $container->get('commerce_easypost.manager'),
      $container->get('commerce_easypost.service_descriptions')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    if (!$this->order->hasField('shipments')) {
      return FALSE;
    }
    return TRUE;
  }

  public function getDisplayLabel() {
    return NULL;
  }

  public function buildPaneSummary() {
    $build = [];
    $carrier_account_number = $this->getCarrierAccountNumber();
    if ($carrier_account_number !== NULL) {
      $build['#title'] = $this->t('Shipping Billed to Carrier Account Number');
      $build['#plain_text'] = $carrier_account_number;
    }
    return $build;
  }

  /**
   * @return string|null
   */
  protected function getCarrierAccountNumber() : ?string {
    if (empty($this->order->shipments)) {
      return NULL;
    }
    foreach ($this->order->shipments->referencedEntities() as $shipment) {
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
      $carrier_account_number = $shipment->get('easypost_carrier_account_number')->value;
      if ($carrier_account_number !== NULL) {
        return $carrier_account_number;
      }
    }
    return NULL;
  }


  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $carrier_account_number = $this->getCarrierAccountNumber();
    $pane_form['easypost_use_customer_account'] = [
      '#type' => 'checkbox',
      '#title' => t('Use your own carrier account'),
      '#default_value' => $carrier_account_number !== NULL,
    ];
    $pane_form['easypost_customer_carrier_account'] = [
      '#type' => 'textfield',
      '#title' => t('Account Number'),
      '#default_value' => $carrier_account_number,
      '#states' => [
        'visible' => [
          ':input[name="commerce_easypost_carrier_account[easypost_use_customer_account]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="commerce_easypost_carrier_account[easypost_use_customer_account]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $settings = [];
    $settings['carrier_names'] = $this->serviceDescriptions->allCarrierNames();
    if ($this->order->hasField('shipments') && !$this->order->get('shipments')->isEmpty()) {
      foreach ($this->order->get('shipments')->referencedEntities() as $shipment) {
        /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
        $shipping_method = $shipment->getShippingMethod();
        $plugin = $shipping_method->getPlugin();
        if (get_class($plugin) === 'Drupal\commerce_easypost\Plugin\Commerce\ShippingMethod\EasyPost') {
          $configuration = $shipment->getShippingMethod()->getPlugin()->getConfiguration();
          foreach ($configuration['enabled_services'] as $carrier => $carrier_settings) {
            if (!empty($carrier_settings['allow_customer_account'])) {
              $settings['shipping_methods'][$shipping_method->id()][$carrier]['allow_customer_account'] = TRUE;
            }
          }
        }
      }
    }
    $pane_form['#attached']['drupalSettings']['easypost_customer_carrier_account'] = $settings;
    $pane_form['#attached']['library'][] = 'commerce_easypost/carrier-account';
    return $pane_form;
  }


  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $values = $form_state->getValue('commerce_easypost_carrier_account');
    $customer_carrier_account = NULL;
    if (!empty($values['easypost_use_customer_account']) && !empty($values['easypost_customer_carrier_account'])) {
      $customer_carrier_account = $values['easypost_customer_carrier_account'];
    }
    // Update the shipments with the customer carrier information
    foreach ($this->order->shipments->referencedEntities() as $shipment) {
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
      $shipment->set('easypost_carrier_account_number', $customer_carrier_account);
      $shipment->save();
    }
  }


}
