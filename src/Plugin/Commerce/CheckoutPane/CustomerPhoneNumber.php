<?php

namespace Drupal\commerce_easypost\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_easypost\Service\EasyPostManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a pane for the customer to enter their own carrier account number.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_easypost_phone",
 *   label = @Translation("Customer Phone Number"),
 *   wrapper_element = "fieldset",
 * )
 */
class CustomerPhoneNumber extends CheckoutPaneBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\commerce_easypost\Service\EasyPostManager
   */
  protected $easyPostManager;

  /**
   * Constructs a new CheckoutPaneBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface $checkout_flow
   *   The parent checkout flow.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager, EasyPostManager $easyPostManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);

    $this->easyPostManager = $easyPostManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow,
      $container->get('entity_type.manager'),
      $container->get('commerce_easypost.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    if (!$this->order->hasField('shipments')) {
      return FALSE;
    }
    return TRUE;
  }

  public function getDisplayLabel() {
    return NULL;
  }

  public function buildPaneSummary() {
    $build = [];
    $phone = $this->getCustomerPhoneNumber();
    if ($phone !== NULL) {
      $build['#title'] = $this->t('Shipping Recipient Phone Number');
      $build['#plain_text'] = $phone;
    }
    return $build;
  }

  /**
   * @return string|null
   */
  protected function getCustomerPhoneNumber() : ?string {
    if (empty($this->order->shipments)) {
      return NULL;
    }
    foreach ($this->order->shipments->referencedEntities() as $shipment) {
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
      $phone = $shipment->get('easypost_phone')->value;
      if ($phone !== NULL) {
        return $phone;
      }
    }
    return NULL;
  }


  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    if (empty($this->order->shipments)) {
      return $pane_form;
    }
    foreach ($this->order->shipments->referencedEntities() as $shipment) {
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
      $plugin = $shipment->getShippingMethod()->getPlugin();
      if ($plugin->getPluginId() === 'easypost' && !empty($plugin->getConfiguration()['options']['collect_customer_phone_number'])) {
        $pane_form['easypost_phone'] = [
          '#type' => 'textfield',
          '#title' => t('Recipient Phone Number'),
          '#required' => TRUE,
          '#default_value' => $this->getCustomerPhoneNumber(),
        ];
      }
      break;
    }
    return $pane_form;
  }


  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $customer_phone = $form_state->getValue('commerce_easypost_phone')['easypost_phone'];
    // Update the shipments with the customer phone information
    foreach ($this->order->shipments->referencedEntities() as $shipment) {
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
      $shipment->set('easypost_phone', $customer_phone);
      $shipment->save();
    }
  }


}
