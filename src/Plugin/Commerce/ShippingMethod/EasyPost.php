<?php

namespace Drupal\commerce_easypost\Plugin\Commerce\ShippingMethod;

use CommerceGuys\Addressing\AddressFormat\AddressField;
use CommerceGuys\Addressing\AddressFormat\FieldOverride;
use Drupal\commerce_easypost\Service\EasyPostManager;
use Drupal\commerce_easypost\Service\EasyPostServiceDescriptions;
use Drupal\commerce_price\RounderInterface;;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\SupportsTrackingInterface;
use Drupal\commerce_shipping\ShipmentItem;
use Drupal\commerce_shipping\ShippingService;
use Drupal\commerce_shipping_label\RemoteLabelFile;
use Drupal\commerce_shipping_label\RemoteShipment;
use Drupal\commerce_shipping_label\ScheduledPickup;
use Drupal\commerce_shipping_label\ScheduledPickupRate;
use Drupal\commerce_shipping_label\ShippingLabelManager;
use Drupal\commerce_shipping_label\SupportsRemoteShipmentsInterface;
use Drupal\commerce_shipping_label\SupportsImportingShippingLabelsInterface;
use Drupal\commerce_shipping_label\SupportsSchedulingPickup;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\state_machine\WorkflowManagerInterface;
use EasyPost\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Provides the EasyPost shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "easypost",
 *   label = @Translation("EasyPost"),
 * )
 */
class EasyPost extends ShippingMethodBase implements SupportsImportingShippingLabelsInterface, SupportsRemoteShipmentsInterface, SupportsTrackingInterface, SupportsSchedulingPickup {

  const DROPOFF_REGULAR_PICKUP = 'REGULAR_PICKUP';
  const DROPOFF_SCHEDULED_PICKUP = 'SCHEDULED_PICKUP';
  const DROPOFF_RETAIL_LOCATION = 'RETAIL_LOCATION';
  const DROPOFF_STATION = 'STATION';
  const DROPOFF_DROP_BOX = 'DROP_BOX';

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The Service Plugins.
   *
   * @var \Drupal\Core\Plugin\DefaultLazyPluginCollection
   */
  protected $plugins;

  /**
   * The price rounder.
   *
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected $rounder;

  /**
   * @var \Drupal\commerce_easypost\Service\EasyPostManager
   */
  protected $easyPostManager;

  /**
   * @var \Drupal\commerce_easypost\Service\EasyPostServiceDescriptions
   */
  protected $serviceDescriptions;

  /**
   * @var \Drupal\commerce_shipping_label\ShippingLabelManager
   */
  protected $shippingLabelManager;

  /**
   * @var FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a new ShippingMethodBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   * @param \Drupal\commerce_fedex\FedExPluginManager $fedex_service_manager
   *   The FedEx Plugin Manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The Event Dispatcher.
   * @param \Drupal\commerce_fedex\FedExRequestInterface $fedex_request
   *   The Fedex Request Service.
   * @param \Psr\Log\LoggerInterface $watchdog
   *   Commerce Fedex Logger Channel.
   * @param \Drupal\commerce_price\RounderInterface $rounder
   *   The price rounder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PackageTypeManagerInterface $package_type_manager, WorkflowManagerInterface $workflow_manager, EventDispatcherInterface $event_dispatcher, RounderInterface $rounder, EasyPostManager $easyPostManager, EasyPostServiceDescriptions $serviceDescriptions, ShippingLabelManager $shippingLabelManager, FileSystemInterface $fileSystem) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager);

    $this->eventDispatcher = $event_dispatcher;
    $this->rounder = $rounder;
    $this->easyPostManager = $easyPostManager;
    $this->serviceDescriptions = $serviceDescriptions;
    $this->shippingLabelManager = $shippingLabelManager;
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow'),
      $container->get('event_dispatcher'),
      $container->get('commerce_price.rounder'),
      $container->get('commerce_easypost.manager'),
      $container->get('commerce_easypost.service_descriptions'),
      $container->get('commerce_shipping_label.manager'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {

    return [
      'api_information' => [
        'api_key' => '',
        'mode' => 'test',
      ],
      'enabled_services' => [],
      'options' => [
        'insurance' => FALSE,
        'rate_multiplier' => 1.0,
        'dropoff_type' => static::DROPOFF_REGULAR_PICKUP,
        'incoterm' => NULL,
        'include_order_number' => FALSE,
        'sender_phone' => NULL,
        'collect_customer_phone_number' => TRUE,
        'round' => PHP_ROUND_HALF_UP,
      ],
      'customs' => [
        'tax_id' => '',
        'send_customs_info' => FALSE,
        'hs_tariff_number' => NULL,
        'description' => NULL,
        'customs_signer' => NULL,
      ],
      'plugins' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['api_information'] = [
      '#type' => 'details',
      '#title' => $this->t('API information'),
      '#description' => $this->isConfigured() ? $this->t('Update your EasyPost API information.') : $this->t('Fill in your EasyPost API information.'),
      '#open' => !$this->isConfigured(),
    ];
    $form['api_information']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#description' => $this->t('Enter your EasyPost API key.'),
      '#default_value' => $this->configuration['api_information']['api_key'],
    ];
    $form['api_information']['mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Mode'),
      '#description' => $this->t('Choose whether to use the test or live mode.'),
      '#options' => [
        'test' => $this->t('Test'),
        'live' => $this->t('Live'),
      ],
      '#default_value' => $this->configuration['api_information']['mode'],
    ];
    $form['enabled_services'] = [
      '#type' => 'details',
      '#title' => $this->t('Services'),
      '#description' => $this->t('Select the services that should be enabled for use.'),
      '#open' => !$this->isConfigured(),
    ];
    $services = $this->serviceDescriptions->getServiceDescriptions();

    foreach ($services as $carrier => $carrier_services) {
      $service_options = [];
      if ($carrier === 'UPSDAP') {
        continue; // This is a duplicate of UPS
      }
      $form['enabled_services'][$carrier] = [
        '#title' => $this->serviceDescriptions->getCarrierName($carrier),
        '#type' => 'details',
      ];
      foreach ($carrier_services as $carrier_service_id => $service_name) {
        // Using the IDs from EasyPost here because some customer-facing descriptions are the same.
        // Example: DHL Express Worldwide and DHL Express Worldwide Non-Doc have same customer-facing
        // description.
        $service_options[$carrier_service_id] = $this->t('@name (@id)', [
          '@name' => $service_name,
          '@id' => $carrier_service_id,
        ]);
      }
      $form['enabled_services'][$carrier]['enabled'] = [
        '#title' => $this->t('Enabled Services'),
        '#type' => 'checkboxes',
        '#options' => $service_options,
        '#default_value' => !empty($this->configuration['enabled_services'][$carrier]['enabled']) ? $this->configuration['enabled_services'][$carrier]['enabled'] : [],
      ];
      $form['enabled_services'][$carrier]['allow_customer_account'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Allow customer to enter their own account number'),
        '#description' => $this->t('Customers choosing this option will have no shipping cost on the order. They will be billed directly from the carrier.'),
        '#default_value' => !empty($this->configuration['enabled_services'][$carrier]['allow_customer_account']) ? $this->configuration['enabled_services'][$carrier]['allow_customer_account'] : FALSE,
      ];
    }
    $form['customs'] = [
      '#type' => 'details',
      '#title' => $this->t('International Customs Settings'),
    ];
    $form['customs']['send_customs_info'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send customs information to EasyPost for international shipments'),
      '#default_value' => $this->configuration['customs']['send_customs_info'],
    ];
    $form['customs']['tax_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sender Tax ID'),
      '#default_value' => $this->configuration['customs']['tax_id'],
    ];
    $form['customs']['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default product description'),
      '#default_value' => $this->configuration['customs']['description'],
    ];
    $form['customs']['hs_tariff_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Harmonized Tariff Schedule Number'),
      '#default_value' => $this->configuration['customs']['hs_tariff_number'],
    ];
    $form['customs']['customs_signer'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Customs Signer'),
      '#default_value' => $this->configuration['customs']['customs_signer'],
    ];
    $form['options']['dropoff_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Dropoff Type'),
      '#description' => $this->t('Choose the method you will use to transfer the package to the carrier.'),
      '#options' => [
        static::DROPOFF_REGULAR_PICKUP => $this->t("Regular Pickup"),
        static::DROPOFF_SCHEDULED_PICKUP => $this->t("Scheduled Pickup"),
        static::DROPOFF_RETAIL_LOCATION => $this->t("Retail Location"),
        static::DROPOFF_STATION => $this->t("Station"),
        static::DROPOFF_DROP_BOX => $this->t("Drop Box"),
      ],
      '#default_value' => $this->configuration['options']['dropoff_type'],
    ];
    $form['options']['incoterm'] = [
      '#type' => 'select',
      '#title' => $this->t('Incoterm'),
      '#description' => $this->t('Incoterm negotiated for shipment.'),
      '#options' => [
        '' => '',
        "EXW" => "EXW",
        "FCA" => "FCA",
        "CPT" => "CPT",
        "CIP" => "CIP",
        "DAT" => "DAT",
        "DAP" => "DAP",
        "DDP" => "DDP",
        "FAS" => "FAS",
        "FOB" => "FOB",
        "CFR" => "CFR",
        "CIF" => "CIF",
      ],
      '#default_value' => $this->configuration['options']['incoterm'],
    ];
    $form['options']['include_order_number'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include order number on label'),
      '#default_value' => $this->configuration['options']['include_order_number'],
    ];
    $form['options']['sender_phone'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sender Phone Number'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['options']['sender_phone'],
    ];
    $form['options']['collect_customer_phone_number'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Collect customer phone number'),
      '#default_value' => $this->configuration['options']['collect_customer_phone_number'],
    ];
    $form['options']['insurance'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include insurance'),
      '#description' => $this->t('Include insurance value of shippable line items in rate requests'),
      '#default_value' => $this->configuration['options']['insurance'],
    ];
    $form['options']['rate_multiplier'] = [
      '#type' => 'number',
      '#title' => $this->t('Rate multiplier'),
      '#description' => $this->t('A number that each rate returned from EasyPost will be multiplied by. For example, enter 1.5 to mark up shipping costs to 150%.'),
      '#min' => 0.1,
      '#step' => 0.1,
      '#size' => 5,
      '#default_value' => $this->configuration['options']['rate_multiplier'],
    ];
    $form['options']['round'] = [
      '#type' => 'select',
      '#title' => $this->t('Round type'),
      '#description' => $this->t('Choose how the shipping rate should be rounded.'),
      '#options' => [
        PHP_ROUND_HALF_UP => 'Half up',
        PHP_ROUND_HALF_DOWN => 'Half down',
        PHP_ROUND_HALF_EVEN => 'Half even',
        PHP_ROUND_HALF_ODD => 'Half odd',
      ],
      '#default_value' => $this->configuration['options']['round'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['api_information']['api_key'] = $values['api_information']['api_key'];
      $this->configuration['api_information']['mode'] = $values['api_information']['mode'];
      $carriers = array_keys($values['enabled_services']);
      $enabled_services = [];
      foreach ($carriers as $carrier) {
        $enabled_services[$carrier]['enabled'] = array_filter($values['enabled_services'][$carrier]['enabled']);
        $enabled_services[$carrier]['allow_customer_account'] = !empty($values['enabled_services'][$carrier]['allow_customer_account']);
        // Add USPDAP duplicate for UPS service
        if ($carrier === 'UPS') {
          $enabled_services['UPSDAP'] = $enabled_services[$carrier];
        }
      }

      $this->configuration['enabled_services'] = $enabled_services;

      $this->configuration['customs']['send_customs_info'] = (bool) $values['customs']['send_customs_info'];
      $this->configuration['customs']['hs_tariff_number'] = $values['customs']['hs_tariff_number'];
      $this->configuration['customs']['description'] = $values['customs']['description'];
      $this->configuration['customs']['customs_signer'] = $values['customs']['customs_signer'];
      $this->configuration['customs']['tax_id'] = $values['customs']['tax_id'];

      $this->configuration['options']['dropoff_type'] = $values['options']['dropoff_type'];
      $this->configuration['options']['incoterm'] = $values['options']['incoterm'];
      $this->configuration['options']['include_order_number'] = (bool) $values['options']['include_order_number'];
      $this->configuration['options']['sender_phone'] = $values['options']['sender_phone'];
      $this->configuration['options']['collect_customer_phone_number'] = $values['options']['collect_customer_phone_number'];
      $this->configuration['options']['rate_multiplier'] = $values['options']['rate_multiplier'];
      $this->configuration['options']['round'] = $values['options']['round'];
    }
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *
   * @return \Drupal\Core\Url|null
   */
  public function getTrackingUrl(ShipmentInterface $shipment) {
    $this->easyPostManager->setConfig($this->configuration, $this->parentEntity->id());
    return $this->easyPostManager->getTrackingUrl($shipment);
  }


  /**
   * {@inheritdoc}
   */
  public function calculateRates(ShipmentInterface $shipment) {
    $this->easyPostManager->setConfig($this->configuration, $this->parentEntity->id());

    if ($shipment->getPackageType() === NULL) {
      $shipment->setPackageType($this->getDefaultPackageType());
    }

    $remote_id = $this->shippingLabelManager->getRemoteShipmentId($shipment);
    $easypost_shipment = NULL;
    if (!empty($remote_id)) {
      try {
        $easypost_shipment = $this->easyPostManager->getEasyPostShipment($remote_id);
      }
      catch (\Exception $e) {
        // We'll handle creating the shipment below.
      }
    }
    if (empty($easypost_shipment)) {
      $easypost_shipment = $this->easyPostManager->createEasyPostShipment($shipment);
    }

    if (!empty($easypost_shipment)) {
      return $this->easyPostManager->getRates($easypost_shipment);
    }
    return [];
  }

  /**
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *
   * @return array
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getRemoteLabelFiles(ShipmentInterface $shipment): array {
    $labels = [];
    $this->easyPostManager->setConfig($this->configuration, $this->parentEntity->id());
    $remote_id = $this->shippingLabelManager->getRemoteShipmentId($shipment);
    if (!empty($remote_id)) {
      $easypost_shipment = $this->easyPostManager->getEasyPostShipment($remote_id, TRUE);
      $formats = [
        'PDF' => ['field_name' => 'label_pdf_url', 'description' => $this->t('PDF')],
        'ZPL' => ['field_name' => 'label_zpl_url', 'description' => $this->t('ZPL')],
        'EPL2' => ['field_name' => 'label_epl2_url', 'description' => $this->t('EPL2')],
        'PNG' => ['field_name' => 'label_url', 'description' => $this->t('PNG')],
      ];
      foreach ($formats as $format => $format_information) {
        if (empty($easypost_shipment->postage_label->{$format_information['field_name']})) {
          try {
            $easypost_shipment->label(['file_format' => $format]);
          }
          catch (Error $error) {
            \Drupal::messenger()->addError($error->getMessage());
            return $labels;
          }
        }
        if (!empty($easypost_shipment->postage_label->{$format_information['field_name']})) {
          $labels[] = new RemoteLabelFile(
            $easypost_shipment->postage_label->{$format_information['field_name']},
            $format_information['description']
          );
        }
      }
      // We should also download the customs forms if there are any.
      if (!empty($easypost_shipment->forms)) {
        $directory = 'private://commerce-easypost';
        $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
        $forms = [];
        foreach ($easypost_shipment->forms as $form) {
          $filename = $this->fileSystem->basename($form->form_url);
          $data = file_get_contents($form->form_url);
          $file = file_save_data($data, $directory . DIRECTORY_SEPARATOR . $filename);
          $forms[] = [
            'target_id' => $file->id(),
            'description' => ucwords(str_replace('_', ' ', $form->form_type)),
          ];
        }
        $shipment->set('easypost_forms', $forms)
          ->save();
      }
    }

    return $labels;
  }

  /**
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *
   * @return \Drupal\commerce_shipping_label\RemoteShipment|null
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function createRemoteShipment(ShipmentInterface $shipment): ?RemoteShipment {
    $this->easyPostManager->setConfig($this->configuration, $this->parentEntity->id());
    $remote_id = $this->shippingLabelManager->getRemoteShipmentId($shipment);
    if (!empty($remote_id)) {
      $easypost_shipment = $this->easyPostManager->getEasyPostShipment($remote_id);
    }
    else {
      $easypost_shipment = $this->easyPostManager->buyShipment($shipment);
    }
    if ($easypost_shipment !== NULL) {
      $remote_shipment = new RemoteShipment();
      $remote_shipment->setId($easypost_shipment->id);
      if (!empty($easypost_shipment->tracker->tracking_code)) {
        $remote_shipment->setTrackingNumber($easypost_shipment->tracker->tracking_code);
      }
      return $remote_shipment;
    }
    return NULL;
  }

  /**
   * @param string $id
   *
   * @return \Drupal\commerce_shipping_label\RemoteShipment|null
   */
  public function getRemoteShipment(string $id): ?RemoteShipment {
    $this->easyPostManager->setConfig($this->configuration, $this->parentEntity->id());
    $easypost_shipment = $this->easyPostManager->getEasyPostShipment($id, TRUE);
    if ($easypost_shipment !== NULL) {
      $remote_shipment = new RemoteShipment();
      $remote_shipment->setId($easypost_shipment->id);
      if (!empty($easypost_shipment->tracker->tracking_code)) {
        $remote_shipment->setTrackingNumber($easypost_shipment->tracker->tracking_code);
      }
      return $remote_shipment;
    }
    return NULL;
  }

  /**
   * @param string $id
   *
   * @return bool
   * @throws \EasyPost\Error
   */
  public function cancelRemoteShipment(string $id) {
    $this->easyPostManager->setConfig($this->configuration, $this->parentEntity->id());
    $easypost_shipment = $this->easyPostManager->getEasyPostShipment($id, TRUE);
    if ($easypost_shipment !== NULL) {
      $easypost_shipment->refund();
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Gets a cleaned title string for use in sending to FedEx.
   *
   * @param \Drupal\commerce_shipping\ShipmentItem $shipment_item
   *   The shipment item.
   *
   * @return string
   *   The cleaned title.
   */
  protected function getCleanTitle(ShipmentItem $shipment_item) {
    $title = $shipment_item->getTitle();
    $title = preg_replace('/[^A-Za-z0-9 ]/', ' ', $title);
    $title = preg_replace('/ +/', ' ', $title);

    return trim($title);
  }

  /**
   * Determine if we have the minimum information to connect to FedEx.
   *
   * @return bool
   *   TRUE if there is enough information to connect, FALSE otherwise.
   */
  protected function isConfigured() {
    $api_information = $this->configuration['api_information'];

    return !empty($api_information['api_key']);
  }

  public function getServices() {
    $return = [];
    $services = $this->serviceDescriptions->getServiceDescriptions();
    foreach ($services as $carrier => $carrier_services) {
      foreach ($carrier_services as $carrier_service => $label) {
        $service_id = $this->serviceDescriptions->getServiceId($carrier, $carrier_service);
        $return[$service_id] = new ShippingService(
          $service_id,
          $label
        );
      }
    }
    return $return;
  }

  public function buildPickupSchedulingForm(array $form, FormStateInterface $formState, ShipmentInterface $shipment): array {

    $form['pickup_window'] = [
      '#tree' => FALSE,
      '#type' => 'fieldset',
      '#title' => $this->t('Pickup window'),
    ];
    $form['pickup_window']['min_datetime'] = [
      '#type' => 'datetime',
      '#title' => $this->t('From'),
      '#required' => TRUE,
      '#default_value' => new DrupalDateTime('today 09:00:00'),
    ];
    $form['pickup_window']['max_datetime'] = [
      '#type' => 'datetime',
      '#title' => $this->t('To'),
      '#required' => TRUE,
      '#default_value' => new DrupalDateTime('today 17:00:00'),
    ];


    $form['address_wrapper'] = [
      '#tree' => FALSE,
      '#type' => 'fieldset',
      '#title' => $this->t('Address'),
    ];
    $form['address_wrapper']['address'] = [
      '#type' => 'address',
      '#title' => $this->t('Pickup address'),
      '#required' => TRUE,
      '#field_overrides' => [
        AddressField::ORGANIZATION => FieldOverride::HIDDEN,
        AddressField::GIVEN_NAME => FieldOverride::HIDDEN,
        AddressField::FAMILY_NAME => FieldOverride::HIDDEN,
      ],
      '#default_value' => $shipment->getOrder()->getStore()->getAddress()->getValue(),
    ];

    $form['address_wrapper']['is_account_address'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('This address is the account on the shipping address'),
      '#default_value' => TRUE,
    ];

    $form['other'] = [
      '#tree' => FALSE,
      '#type' => 'fieldset',
      '#title' => $this->t('Other Information'),
    ];
    $form['other']['instructions'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pickup Instructions'),
      '#description' => $this->t('Any special instructions for the carrier.'),
      '#rows' => 2,
    ];
    return $form;
  }

  public function validatePickupSchedulingForm(array $form, FormStateInterface $formState, ShipmentInterface $shipment): void {
    // No extra validation needed.
  }

  public function buildScheduledPickupFromFormSubmit(array $form, FormStateInterface $formState, ScheduledPickup $pickup): ScheduledPickup {
    $values = $formState->getValues();
    if (!empty($values['instructions'])) {
      $pickup->setDataValue('instructions', $values['instructions']);
    }
    if (!empty($values['min_datetime'])) {
      $pickup->setDataValue('min_datetime', $values['min_datetime']->format('c'));
    }
    if (!empty($values['max_datetime'])) {
      $pickup->setDataValue('max_datetime', $values['max_datetime']->format('c'));
    }
    if (!empty($values['is_account_address'])) {
      $pickup->setDataValue('is_account_address', TRUE);
    }
    else {
      $pickup->setDataValue('is_account_address', FALSE);
    }
    $pickup->setDataValue('address', $values['address']);
    return $pickup;
  }

  public function getPickupRates(ScheduledPickup $pickup) : array {
    $this->easyPostManager->setConfig($this->configuration, $this->parentEntity->id());
    return $this->easyPostManager->getPickupRates($pickup);
  }

  public function schedulePickup(ScheduledPickup $pickup, ScheduledPickupRate $selectedRate): ScheduledPickup {
    $this->easyPostManager->setConfig($this->configuration, $this->parentEntity->id());
    try {
      return $this->easyPostManager->schedulePickup($pickup, $selectedRate);
    }
    catch (\EasyPost\Error $e) {
      \Drupal::messenger()->addError($e->getMessage());
      return $pickup;
    }
  }

  public function cancelPickup(string $pickup_remote_id): bool {
    $this->easyPostManager->setConfig($this->configuration, $this->parentEntity->id());
    try {
      return $this->easyPostManager->cancelPickup($pickup_remote_id);
    }
    catch (\EasyPost\Error $e) {
      \Drupal::messenger()->addError($e->getMessage());
      return FALSE;
    }
  }

}
