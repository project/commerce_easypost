<?php

namespace Drupal\commerce_easypost\Event;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * @see \Drupal\commerce_easypost\Event\EasyPostEvents
 */
class EasyPostShipmentEvent extends Event {

  /**
   * The shipment.
   *
   * @var \Drupal\commerce_shipping\Entity\ShipmentInterface
   */
  protected $shipment;

  /**
   * @var array
   */
  protected $values;

  /**
   * EasyPostShipmentEvent constructor.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   * @param array $values
   */
  public function __construct(ShipmentInterface $shipment, array $values) {
    $this->shipment = $shipment;
    $this->values = $values;
  }

  /**
   * @return \Drupal\commerce_shipping\Entity\ShipmentInterface
   */
  public function getShipment(): ShipmentInterface {
    return $this->shipment;
  }

  /**
   * @return array
   */
  public function getValues(): array {
    return $this->values;
  }

  /**
   * @param array $values
   *
   * @return EasyPostShipmentEvent
   */
  public function setValues(array $values): EasyPostShipmentEvent {
    $this->values = $values;
    return $this;
  }

}
