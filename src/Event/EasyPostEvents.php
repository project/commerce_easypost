<?php

namespace Drupal\commerce_easypost\Event;

class EasyPostEvents {

  /**
   * @Event
   *
   * @see \Drupal\commerce_easypost\Event\EasyPostShipmentEvent
   */
  const SHIPMENT_PRE_CREATE = 'commerce_easypost.shipment.pre_create';

}
