<?php

namespace Drupal\commerce_easypost\EventSubscriber;

use Drupal\commerce_shipping_label\Event\ShippingLabelEvents;
use Drupal\commerce_shipping_label\Event\ShippingLabelListEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


class ShippingLabelListSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  protected $viewBuilder;

  /**
   * ShippingLabelListSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->viewBuilder = $this->entityTypeManager->getViewBuilder('commerce_shipment');
  }


  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      ShippingLabelEvents::SHIPMENT_LIST_BUILDER_HEADER => ['addFormsHeader', 50],
      ShippingLabelEvents::SHIPMENT_LIST_BUILDER_ROW => ['addFormsColumn', 50],
    ];

    return $events;
  }


  public function addFormsHeader(ShippingLabelListEvent $event) {
    $header = $event->getData();
    $header = array_slice($header, 0, count($header) - 1, TRUE) +
      ['easypost_forms' => $this->t('Forms')] +
      array_slice($header, count($header) - 1, 1, TRUE);
    $event->setData($header);
  }

  public function addFormsColumn(ShippingLabelListEvent $event) {
    $row = $event->getData();
    $forms = $this->viewBuilder->viewField($event->getShipment()->get('easypost_forms'), ['label' => 'hidden']);
    $row = array_slice($row, 0, count($row) - 1, TRUE) +
      ['easypost_forms' => render($forms)] +
      array_slice($row, count($row) - 1, 1, TRUE);
    $event->setData($row);
  }

}
