<?php

namespace Drupal\commerce_easypost\Service;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class EasyPostServiceDescriptions {

  use StringTranslationTrait;

  /**
   * @param string|null $carrier
   *
   * @return string[]
   */
  public function getServiceDescriptions(?string $carrier = NULL) : array {
    $descriptions = $this->allDescriptions();
    if ($carrier !== NULL) {
      return $descriptions[$carrier] ?? [];
    }
    return $descriptions;
  }

  /**
   * @param string $carrier
   * @param string $service
   *
   * @return string
   */
  public function getServiceId(string $carrier, string $service) : string {
    return 'EasyPost::' . $carrier . '::' . $service;
  }

  /**
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *
   * @return string|null
   */
  public function getServiceDescriptionForShipment(ShipmentInterface $shipment) : ?string {
    if (!empty($shipment->getShippingService()) && preg_match('/^EasyPost::(.*)::(.*)$/', $shipment->getShippingService(), $matches)) {
      return $this->getServiceName($matches[1], $matches[2]);
    }
    return NULL;
  }

  /**
   * @param string $carrier
   * @param string $service
   *
   * @return string|null
   */
  public function getServiceName(string $carrier, string $service) : ?string {
    $descriptions = $this->getServiceDescriptions($carrier);
    return $descriptions[$service] ?? NULL;
  }

  public function getCarrierName(string $carrier) : ?string {
    $carrier_names = $this->allCarrierNames();
    return $carrier_names[$carrier] ?? NULL;
  }

  public function allCarrierNames() : array {
    return [
      'DHLExpress' => $this->t('DHL Express'),
      'FedEx' => $this->t('FedEx'),
      'UPS' => $this->t('UPS'),
      'UPSDAP' => $this->t('UPS'),
      'USPS' => $this->t('USPS'),
    ];
  }

  /**
   * @return array[]
   */
  protected function allDescriptions() : array {
    return [
      'DHLExpress' => [
        'BreakBulkEconomy' => $this->t('DHL Bulk Economy'),
        'BreakBulkExpress' => $this->t('DHL Bulk Express'),
        'DomesticEconomySelect' => $this->t('DHL Domestic Economy Select'),
        'DomesticExpress' => $this->t('DHL Domestic Express'),
        'DomesticExpress1030' => $this->t('DHL Domestic Express 10:30'),
        'DomesticExpress1200' => $this->t('DHL Domestic Express 12:00'),
        'EconomySelect' => $this->t('DHL Economy Select'),
        'EconomySelectNonDoc' => $this->t('DHL Economy Select'),
        'EuroPack' => $this->t('DHL EuroPack'),
        'EuropackNonDoc' => $this->t('DHL EuroPack'),
        'Express1030' => $this->t('DHL Express 10:30'),
        'Express1030NonDoc' => $this->t('DHL Express 10:30'),
        'Express1200NonDoc' => $this->t('DHL Express 12:00'),
        'Express1200' => $this->t('DHL Express 12:00'),
        'Express900' => $this->t('DHL Express 9:00'),
        'Express900NonDoc' => $this->t('DHL Express 9:00'),
        'ExpressEasy' => $this->t('DHL Express Easy'),
        'ExpressEasyNonDoc' => $this->t('DHL Express Easy'),
        'ExpressEnvelope' => $this->t('DHL Express Envelope'),
        'ExpressWorldwide' => $this->t('DHL Express Worldwide'),
        'ExpressWorldwideB2C' => $this->t('DHL Express Worldwide'),
        'ExpressWorldwideB2CNonDoc' => $this->t('DHL Express Worldwide'),
        'ExpressWorldwideECX' => $this->t('DHL Express Worldwide'),
        'ExpressWorldwideNonDoc' => $this->t('DHL Express Worldwide'),
        'FreightWorldwide' => $this->t('DHL Freight Worldwide'),
        'GlobalmailBusiness' => $this->t('DHL Globalmail Business'),
        'JetLine' => $this->t('DHL Jet Line'),
        'JumboBox' => $this->t('DHL Jumbo Box'),
        'LogisticsServices' => $this->t('DHL Logistics Services'),
        'SameDay' => $this->t('DHL Same Day'),
        'SecureLine' => $this->t('DHL Secure Line'),
        'SprintLine' => $this->t('DHL Sprint Line'),
      ],
      'FedEx' => [
        'FEDEX_GROUND' => $this->t('FedEx Ground'),
        'FEDEX_2_DAY' => $this->t('FedEx Second Day'),
        'FEDEX_2_DAY_AM' => $this->t('FedEx Second Day AM'),
        'FEDEX_EXPRESS_SAVER' => $this->t('FedEx Express Saver'),
        'STANDARD_OVERNIGHT' => $this->t('FedEx Standard Overnight'),
        'FIRST_OVERNIGHT' => $this->t('FedEx First Overnight'),
        'PRIORITY_OVERNIGHT' => $this->t('FedEx Priority Overnight'),
        'INTERNATIONAL_ECONOMY' => $this->t('FedEx International Economy'),
        'INTERNATIONAL_FIRST' => $this->t('FedEx International First'),
        'INTERNATIONAL_PRIORITY' => $this->t('FedEx International Priority'),
        'GROUND_HOME_DELIVERY' => $this->t('FedEx Ground Home Delivery'),
        'SMART_POST' => $this->t('FedEx Smart Post'),
      ],
      'UPS' => [
        'Ground' => $this->t('UPS Ground'),
        'UPSStandard' => $this->t('UPS Standard'),
        'UPSSaver' => $this->t('UPS Saver'),
        'Express' => $this->t('UPS Worldwide Express'),
        'ExpressPlus' => $this->t('UPS Worldwide Express Plus'),
        'Expedited' => $this->t('UPS Worldwide Expedited'),
        'NextDayAir' => $this->t('UPS Next Day Air'),
        'NextDayAirSaver' => $this->t('UPS Next Day Air Saver'),
        'NextDayAirEarlyAM' => $this->t('UPS Next Day Air Early AM'),
        '2ndDayAir' => $this->t('UPS Second Day Air'),
        '2ndDayAirAM' => $this->t('UPS Second Day Air AM'),
        '3DaySelect' => $this->t('UPS Three-Day Select'),
      ],
      'UPSDAP' => [
        'Ground' => $this->t('UPS Ground'),
        'UPSStandard' => $this->t('UPS Standard'),
        'UPSSaver' => $this->t('UPS Saver'),
        'Express' => $this->t('UPS Worldwide Express'),
        'ExpressPlus' => $this->t('UPS Worldwide Express Plus'),
        'Expedited' => $this->t('UPS Worldwide Expedited'),
        'NextDayAir' => $this->t('UPS Next Day Air'),
        'NextDayAirSaver' => $this->t('UPS Next Day Air Saver'),
        'NextDayAirEarlyAM' => $this->t('UPS Next Day Air Early AM'),
        '2ndDayAir' => $this->t('UPS Second Day Air'),
        '2ndDayAirAM' => $this->t('UPS Second Day Air AM'),
        '3DaySelect' => $this->t('UPS Three-Day Select'),
      ],
      'USPS' => [
        'First' => $this->t('USPS First Class'),
        'Priority' => $this->t('USPS Priority'),
        'Express' => $this->t('USPS Express'),
        'ParcelSelect' => $this->t('USPS Parcel Select'),
        'LibraryMail' => $this->t('USPS Library Mail'),
        'MediaMail' => $this->t('USPS Media Mail'),
        'FirstClassMailInternational' => $this->t('USPS First Class International'),
        'FirstClassPackageInternationalService' => $this->t('USPS First Class Package International'),
        'PriorityMailInternational' => $this->t('USPS Priority Mail International'),
        'ExpressMailInternational' => $this->t('USPS Express Mail International'),
      ],
    ];
  }

}
