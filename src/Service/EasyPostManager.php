<?php

namespace Drupal\commerce_easypost\Service;

use Drupal\commerce_easypost\Event\EasyPostEvents;
use Drupal\commerce_easypost\Event\EasyPostShipmentEvent;
use Drupal\commerce_price\Price;
use Drupal\commerce_price\RounderInterface;
use Drupal\commerce_shipping_label\ScheduledPickup;
use Drupal\commerce_shipping_label\ScheduledPickupRate;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_shipping\ShippingService;
use Drupal\commerce_shipping_label\ShippingLabelManager;
use Drupal\Core\Url;
use EasyPost\EasyPost;
use EasyPost\Error;
use EasyPost\Shipment;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class EasyPostManager {

  /**
   * The configuration array from a CommerceShippingMethod.
   *
   * @var array
   */
  protected $configuration;

  /**
   * @var string
   */
  protected $shippingMethodId;

  /**
   * The price rounder.
   *
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected $rounder;

  /**
   * @var \Drupal\commerce_easypost\Service\EasyPostServiceDescriptions
   */
  protected $serviceDescriptions;

  /**
   * @var \EasyPost\Shipment[]
   */
  protected $easypostShipments;

  /**
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * @var \Drupal\commerce_shipping_label\ShippingLabelManager
   */
  protected $shippingLabelManager;

  /**
   * EasyPostManager constructor.
   *
   * @param \Drupal\commerce_price\RounderInterface $rounder
   */
  public function __construct(RounderInterface $rounder, EasyPostServiceDescriptions $serviceDescriptions, EventDispatcherInterface $eventDispatcher, ShippingLabelManager $shippingLabelManager) {
    $this->rounder = $rounder;
    $this->serviceDescriptions = $serviceDescriptions;
    $this->eventDispatcher = $eventDispatcher;
    $this->easypostShipments = [];
    $this->shippingLabelManager = $shippingLabelManager;
  }


  public function setConfig(array $configuration, string $shippingMethodId) {
    $this->configuration = $configuration;
    $this->shippingMethodId = $shippingMethodId;
    \EasyPost\EasyPost::setApiKey($this->configuration['api_information']['api_key']);
  }


  /**
   * @param \EasyPost\Shipment $easypost_shipment
   *
   * @return ShippingRate[]
   */
  public function getRates(Shipment $easypost_shipment) : array {

    $rates = [];
    if (!empty($easypost_shipment->rates)) {
      $multiplier = (!empty($this->configuration['options']['rate_multiplier']))
        ? $this->configuration['options']['rate_multiplier']
        : 1.0;
      $round = !empty($this->configuration['options']['round'])
        ? $this->configuration['options']['round']
        : PHP_ROUND_HALF_UP;
      $enabled_services = $this->configuration['enabled_services'];
      foreach ($easypost_shipment->rates as $rate) {
        if (!empty($enabled_services[$rate->carrier]['enabled'][$rate->service])) {
          $service = new ShippingService(
            $this->serviceDescriptions->getServiceId($rate->carrier, $rate->service),
            $this->serviceDescriptions->getServiceName($rate->carrier, $rate->service)
          );
          $price = new Price((string) $rate->rate, $rate->currency);
          if ($multiplier != 1) {
            $price = $price->multiply((string) $multiplier);
          }
          $price = $this->rounder->round($price, $round);

          $commerce_rate = new ShippingRate([
            'shipping_method_id' => $this->shippingMethodId,
            'service' => $service,
            'amount' => $price,
          ]);
          $commerce_rate->easypost_rate = $rate;
          $rates[] = $commerce_rate;
        }
      }
    }
    return $rates;
  }

  public function buyShipment(ShipmentInterface $shipment) : \EasyPost\Shipment {
    $easypost_shipment = $this->createEasyPostShipment($shipment);
    $rates = $this->getRates($easypost_shipment);
    foreach ($rates as $rate) {
      if ($rate->getService()->getId() === $shipment->getShippingService()) {
        if (!empty($rate->easypost_rate)) {
          try {
            $easypost_shipment->buy([
              'rate' => $rate->easypost_rate,
            ]);
          }
          catch (Error $error) {
            \Drupal::messenger()->addError($error->getMessage());
          }
        }
      }
    }
    return $easypost_shipment;
  }

  /**
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *
   * @return \EasyPost\Shipment|null
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function createEasyPostShipment(ShipmentInterface $shipment) : ?\EasyPost\Shipment {
    if ($shipment->getShippingProfile()->address->isEmpty()) {
      return NULL;
    }

    /** @var \CommerceGuys\Addressing\AddressInterface $address */
    $address = $shipment->getOrder()->getStore()->getAddress();
    $from_address = \EasyPost\Address::create(
      array(
        "company" => $shipment->getOrder()->getStore()->getName(),
        "street1" => $address->getAddressLine1(),
        "street2" => $address->getAddressLine2(),
        "city"    => $address->getLocality(),
        "state"   => $address->getAdministrativeArea(),
        "zip"     => $address->getPostalCode(),
        "country" => $address->getCountryCode(),
        'phone' => $this->configuration['options']['sender_phone'],
        'federal_tax_id' => $this->configuration['customs']['tax_id']
      )
    );
    /** @var \CommerceGuys\Addressing\AddressInterface $address */
    $address = $shipment->getShippingProfile()->get('address')->first();
    $to_address = \EasyPost\Address::create(
      array(
        "company" => $address->getOrganization(),
        "street1" => $address->getAddressLine1(),
        "street2" => $address->getAddressLine2(),
        "city"    => $address->getLocality(),
        "state"   => $address->getAdministrativeArea(),
        "zip"     => $address->getPostalCode(),
        "country" => $address->getCountryCode(),
        'verify' => ['delivery', 'zip4'],
        'phone' => $shipment->get('easypost_phone')->value,
      )
    );


    $parcel = \EasyPost\Parcel::create(
      array(
        "length" => $shipment->getPackageType()->getLength()->convert('in')->getNumber(),
        "height" => $shipment->getPackageType()->getHeight()->convert('in')->getNumber(),
        "width" => $shipment->getPackageType()->getWidth()->convert('in')->getNumber(),
        "weight" => $shipment->getWeight()->convert('oz')->getNumber(),
      )
    );
    $options = [
      'dropoff_type' => $this->configuration['options']['dropoff_type'],
      'incoterm' => $this->configuration['options']['incoterm'],
    ];
    if (!empty($this->configuration['options']['include_order_number']) && !empty($shipment->getOrder()->getPlacedTime())) {
      $options['invoice_number'] = $shipment->getOrder()->getOrderNumber();
      $options['print_custom_1'] = $shipment->getOrder()->getOrderNumber();
      $options['print_custom_1_code'] = 'IK';
    }
    $options = array_filter($options);

    /** @var \CommerceGuys\Addressing\AddressInterface $billing_address */
    if ($shipment->getOrder()->getBillingProfile() !== NULL) {
      $billing_address = $shipment->getOrder()->getBillingProfile()->get('address')->first();
    }
    $store_address = $shipment->getOrder()->getStore()->getAddress();

    $carrier_account_number = $this->getCarrierAccountNumber($shipment);
    if (!empty($carrier_account_number)) {
      $options['payment'] = [
        'type' => 'RECEIVER',
        'account' => $carrier_account_number,
        'postal_code' => $billing_address->getPostalCode(),
      ];
    }
    if ($this->isInternational($shipment)) {
      $customs_items = [];
      $total_items = 0;
      foreach ($shipment->getItems() as $item) {
        $total_items += (float) $item->getQuantity();
      }
      $weight_per_item = ((float) $shipment->getWeight()->convert('oz')->getNumber()) / $total_items;
      foreach ($shipment->getItems() as $item) {
        $customs_item_info = [
          'quantity' => (float) $item->getQuantity(),
          'weight' => $weight_per_item * $item->getQuantity(),
          'value' => $item->getDeclaredValue()->getNumber(),
          'currency' => $item->getDeclaredValue()->getCurrencyCode(),
          'origin_country' => $store_address->getCountryCode(),
        ];
        $customs_item_info['description'] = !empty($this->configuration['customs']['description']) ? $this->configuration['customs']['description'] : $this->t('Merchandise');
        if (!empty($this->configuration['customs']['hs_tariff_number'])) {
          $customs_item_info['hs_tariff_number'] = $this->configuration['customs']['hs_tariff_number'];
        }
        $customs_items[] = $customs_item_info;
      }
      $customs_info = [
        'contents_type' => 'merchandise',
        'customs_items' => $customs_items,
        'customs_certify' => true,
        'customs_signer' => !empty($this->configuration['customs']['customs_signer']) ? $this->configuration['customs']['customs_signer'] : '',
        'restriction_type' => 'none',
      ];
      if ($shipment->hasField('easypost_aes_itn') && !$shipment->get('easypost_aes_itn')->isEmpty()) {
        $customs_info['eel_pfc'] = $shipment->get('easypost_aes_itn')->value;
      }
      else {
        $customs_info['eel_pfc'] = 'NOEEI 30.37(a)';
      }
      //$customs_info = \EasyPost\CustomsInfo::create(
      //  $customs_info
      //);
    }
    $values = [
      "to_address"   => $to_address,
      "from_address" => $from_address,
      "parcel"       => $parcel,
      'options'      => $options,
    ];
    if (isset($customs_info)) {
      $values['customs_info'] = $customs_info;
    }

    $event = new EasyPostShipmentEvent($shipment, $values);
    $this->eventDispatcher->dispatch(EasyPostEvents::SHIPMENT_PRE_CREATE, $event);
    $values = $event->getValues();
    $easypost_shipment = \EasyPost\Shipment::create(
      $values
    );
    return $easypost_shipment;
  }

  public function getTrackingUrl(ShipmentInterface $shipment) {
    $remote_id = $this->shippingLabelManager->getRemoteShipmentId($shipment);
    if (!empty($remote_id)) {
      try {
        $easypost_shipment = $this->getEasyPostShipment($remote_id);
        if ($easypost_shipment !== NULL && !empty($easypost_shipment->tracker) && !empty($easypost_shipment->tracker->public_url)) {
          return Url::fromUri($easypost_shipment->tracker->public_url);
        }
      }
      catch (\EasyPost\Error $e) {
        return NULL;
      }
    }
    return NULL;
  }

  /**
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *
   * @return \Drupal\commerce_price\Price
   */
  protected function getTotalItemPrice(ShipmentInterface $shipment) : Price {
    $total = new Price(0, 'USD');
    foreach ($shipment->getOrder()->getItems() as $item) {
      $total = $total->add($item->getAdjustedUnitPrice());
    }
    return $total;
  }

  /**
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *
   * @return bool
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function isInternational(ShipmentInterface $shipment) {
    $shipping_address = NULL;
    if ($shipment->getShippingProfile() !== NULL) {
      $shipping_address = $shipment->getShippingProfile()->get('address')->first();
    }
    $store_address = $shipment->getOrder()->getStore()->getAddress();
    return $shipping_address !== NULL && ($shipping_address->getCountryCode() !== $store_address->getCountryCode());
  }

  /**
   * @return string|null
   */
  public function getCarrierAccountNumber(ShipmentInterface $shipment) : ?string {
    return preg_replace('/\D/', '', $shipment->get('easypost_carrier_account_number')->value);
  }

  /**
   * @param string $easypost_shipment_id
   *
   * @return \EasyPost\Shipment
   */
  public function getEasyPostShipment(string $easypost_shipment_id, $reset = FALSE) : ?\EasyPost\Shipment {
    if ($reset || !isset($this->easypostShipments[$easypost_shipment_id])) {
      try {
        $this->easypostShipments[$easypost_shipment_id] = \EasyPost\Shipment::retrieve($easypost_shipment_id);
      }
      catch (\Exception $e) {
        return NULL;
      }
    }
    return $this->easypostShipments[$easypost_shipment_id];
  }

  public function getPickupRates(ScheduledPickup $pickup) : array {
    $pickup_rates = [];
    $shipment = $pickup->getShipment();
    if (empty($shipment->get('shipment_label_remote_id')->value)) {
      return $pickup_rates;
    }
    $easypost_shipment = \EasyPost\Shipment::retrieve($shipment->get('shipment_label_remote_id')->value);


    $values = [
      'shipment' => $easypost_shipment,
      'is_account_address' => $pickup->getDataValue('is_account_address'),
      'min_datetime' => $pickup->getDataValue('min_datetime'),
    ];

    $shipping_rate = NULL;

    foreach ($this->getRates($easypost_shipment) as $rate) {
      if ($rate->getService()->getId() === $shipment->getShippingService()) {
        $shipping_rate = $rate;
      }
    }
    if ($shipping_rate !== NULL) {
      $values['carrier_accounts'] = ['id' => $shipping_rate->easypost_rate->carrier_account_id];
    }


    if ($pickup->getDataValue('instructions') !== NULL) {
      $values['instructions'] = $pickup->getDataValue('instructions');
    }
    if ($pickup->getDataValue('max_datetime') !== NULL) {
      $values['max_datetime'] = $pickup->getDataValue('max_datetime');
    }
    if ($pickup->getDataValue('address') !== NULL) {
      $address = $pickup->getDataValue('address');
      $easypost_address = \EasyPost\Address::create([
        "company" => $shipment->getOrder()->getStore()->getName(),
        "street1" => $address['address_line1'],
        "street2" => $address['address_line2'],
        "city"    => $address['locality'],
        "state"   => $address['administrative_area'],
        "zip"     => $address['postal_code'],
        "country" => $address['country_code'],
        'phone' => $this->configuration['options']['sender_phone'],
      ]);
      $values['address'] = $easypost_address;
    }

    $easypost_pickup = \EasyPost\Pickup::create($values);
    $pickup->setRemoteId($easypost_pickup->id);
    if (!empty($easypost_pickup->pickup_rates)) {
      foreach ($easypost_pickup->pickup_rates as $easypost_pickup_rate) {
        $pickup_rate = new ScheduledPickupRate();
        $pickup_rate->setDescription($easypost_pickup_rate->carrier . ' ' . $easypost_pickup_rate->service)
          ->setPrice(new Price($easypost_pickup_rate->rate, $easypost_pickup_rate->currency))
          ->setRemoteId($easypost_pickup_rate->id)
          ->setDataValue('easypost_rate', $easypost_pickup_rate);
        $pickup_rates[] = $pickup_rate;
      }
    }

    return $pickup_rates;
  }

  public function schedulePickup(ScheduledPickup $pickup, ScheduledPickupRate $selectedRate) {
    $easypost_pickup = \EasyPost\Pickup::retrieve($pickup->getRemoteId());
    $easypost_pickup->buy($selectedRate->getDataValue('easypost_rate'));
    if (!empty($easypost_pickup->confirmation)) {
      $pickup->setConfirmationNumber($easypost_pickup->confirmation);
    }
    if (!empty($easypost_pickup->status) && $easypost_pickup->status !== 'unknown') {
      $pickup->setStatus($easypost_pickup->status);
    }
    $pickup->setRemoteId($easypost_pickup->id);
    return $pickup;
  }

  public function cancelPickup(string $pickup_remote_id) : bool {
    $easypost_pickup = \EasyPost\Pickup::retrieve($pickup_remote_id);
    try {
      $easypost_pickup = $easypost_pickup->cancel($pickup_remote_id);
      return TRUE;
    }
    catch (\Exception | \Error $e) {
      \Drupal::messenger()->addError($e->getMessage());
      return FALSE;
    }
  }
}
