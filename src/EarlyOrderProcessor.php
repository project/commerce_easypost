<?php

namespace Drupal\commerce_easypost;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\OrderProcessorInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_order\Adjustment;
use Drupal\commerce_shipping\ShippingOrderManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Prepares shipments for the order refresh process.
 *
 * Runs before other order processors (promotion, tax, etc).
 * Packs the shipments, resets their amounts and adjustments.
 *
 * Once the other order processors perform their changes, the
 * LateOrderProcessor transfers the shipment adjustments to the order.
 *
 * @see \Drupal\commerce_shipping\LateOrderProcessor
 */
class EarlyOrderProcessor implements OrderProcessorInterface {

  use StringTranslationTrait;

  /**
   * The shipping order manager.
   *
   * @var \Drupal\commerce_shipping\ShippingOrderManagerInterface
   */
  protected $shippingOrderManager;

  /**
   * Constructs a new EarlyOrderProcessor object.
   *
   * @param \Drupal\commerce_shipping\ShippingOrderManagerInterface $shipping_order_manager
   *   The shipping order manager.
   */
  public function __construct(ShippingOrderManagerInterface $shipping_order_manager) {
    $this->shippingOrderManager = $shipping_order_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function process(OrderInterface $order) {
    if (!$this->shippingOrderManager->hasShipments($order)) {
      return;
    }

    /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface[] $shipments */
    $shipments = $order->get('shipments')->referencedEntities();
    foreach ($shipments as $key => $shipment) {
      if ($shipment->get('easypost_carrier_account_number')->value !== NULL) {
        $amount = $shipment->getAmount();
        $shipment->setAmount(new Price(0, $amount->getCurrencyCode()));
        $shipment->addAdjustment(new Adjustment([
          'type' => 'commerce_easypost_custom_carrier_account',
          'label' => $this->t('Ship with customer carrier account'),
          'amount' => $amount->multiply('-1'),
          'source_id' => 'shipping_custom_carrier_account',
          'included' => TRUE,
        ]));
      }
    }
  }

}
